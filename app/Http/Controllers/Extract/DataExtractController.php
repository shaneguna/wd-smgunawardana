<?php

namespace App\Http\Controllers\Extract;

use Illuminate\Http\Request;

use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;

class DataExtractController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function extractFormInfo(Request $request)
  {

   $info = User::all();

         Excel::create('data_extract', function($excel) use ($info){

             $excel->setTitle('Data Extract');
             $excel->setCreator('sven')
                   ->setCompany('sven');
             $excel->sheet('users', function($sheet) use ($info){
             $sheet->getStyle('A1')->applyFromArray(array(
                         'fill'        => array(
                         'color'       => array('rgb' => 'FF0000'),
                         'font-weight' => "bold")
                 ));
                 $sheet->loadView('extract.extract')
                     ->with('list',$info);
             });
         })->export('xls');

    return view('home');

 }

}
