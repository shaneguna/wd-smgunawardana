<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<h2></h2>
<table>
    <thead>
    <tr style="font-weight: bold;text-align: center;font-size: 15px">
        <td style="font-weight: bold;text-align: center;font-size: 15px" colspan="6">Users List</td>
    </tr>

    </thead>
</table>
<table>
    <thead>
        <tr style="font-weight: bold;text-align: center;font-size: 15px">
            <td>User</td>
            <td>Email</td>
            <td>Birth Date</td>
        </tr>
    </thead>
    <tbody>
        @forelse($list as $l)
            <tr style="font-size: 12px">
                <td>{{$l->name}}</td>
                <td>{{$l->email}}</td>
                <td>{{$l->birthdate}}</td>
            </tr>
            @empty
            <tr colspan="8"> No records found</tr>
        @endforelse
    </tbody>
</table>
</html>
