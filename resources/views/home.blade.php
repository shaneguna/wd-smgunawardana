@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">

                    <form class="form-horizontal" role="form" method="GET" action="{{ url('/extract-data') }}">
                      <center>  You are logged in!
                        <br>
                        <button type="submit" class="btn btn-primary">
                          Download
                        </button>
                        </br>
                      </center>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
